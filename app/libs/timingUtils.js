export const momentToDate = (time) => {
  let day = addZero(time.date())
  let month = addZero(time.month())
  let hours = addZero(time.hour())
  let minutes = addZero(time.minute())
  let seconds = addZero(time.second())

  let completeDate = day+'/'+month+', '+hours + ':' + minutes + ":" + seconds
  return completeDate
}

const addZero = (val) => {
  return (val < 10) ? ('0' + val) : val
}
