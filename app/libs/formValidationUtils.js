export const checkFormValidations = (self, formName) => {
  let ret = false
  let form = self.refs[formName]
  if(form && form.getForm()){
    ret = form.getForm().getValidationState()
  }
  return ret
}
