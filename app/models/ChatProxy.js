'use strict'

import EventEmitter from 'events'
import io from 'socket.io-client'
import Peer from 'peerjs'

function getChatProxyInstance(){
  let instance = new ChatProxy()
  window.chatProxyInstance = instance
  return instance
}

class ChatProxy extends EventEmitter {
  constructor(){
    super()

    this.setUsername = this.setUsername.bind(this)
    this.getUsername = this.getUsername.bind(this)
    this.setCamRef = this.setCamRef.bind(this)
    this.getCamRef = this.getCamRef.bind(this)
    this.onMessage = this.onMessage.bind(this)
    this.onPrivateMessage = this.onPrivateMessage.bind(this)
    this.onUsersList = this.onUsersList.bind(this)
    this.onMessagesList = this.onMessagesList.bind(this)
    this.saveSnapDictionary = this.saveSnapDictionary.bind(this)
    this.onStartCall = this.onStartCall.bind(this)
    this.onCloseCall = this.onCloseCall.bind(this)
    this.updateSnapDictionary = this.updateSnapDictionary.bind(this)
    this.onUserConnected = this.onUserConnected.bind(this)
    this.onUserDisconnected = this.onUserDisconnected.bind(this)
    this.onChangeAvatar = this.onChangeAvatar.bind(this)
    this.onChangeAvailability = this.onChangeAvailability.bind(this)
    this.requestSnap = this.requestSnap.bind(this)
    this.broadcastMessage = this.broadcastMessage.bind(this)
    this.sendPrivateMessage = this.sendPrivateMessage.bind(this)
    this.broadcastAvatarChanged = this.broadcastAvatarChanged.bind(this)
    this.disconnect = this.disconnect.bind(this)
    this.connectToServer = this.connectToServer.bind(this)
    this.prepareEntryDictionary = this.prepareEntryDictionary.bind(this)
    this.registerCallListeners = this.registerCallListeners.bind(this)
    this._connectTo = this._connectTo.bind(this)
    this._registerPeer = this._registerPeer.bind(this)
    this.requestStartCall = this.requestStartCall.bind(this)
    this.requestCloseCall = this.requestCloseCall.bind(this)
    this.takeSnapshot = this.takeSnapshot.bind(this)
    this._disconnectFrom = this._disconnectFrom.bind(this)

    this._peers = {}
    this.id = Math.random()
  }

  setUsername(username) {
    this._username = username
  }

  getUsername () {
    return this._username
  }

  setCamRef(camRef) {
    this.camRef = camRef
  }

  getCamRef () {
    return this.camRef
  }


  //---------------- Handlers per gli eventi ricevuti dal server ----------------
  onMessage (cb) {
    this.addListener('NEW_MESSAGE', cb)
  }

  onPrivateMessage (cb) {
    this.addListener('NEW_PRIVATE_MESSAGE', cb)
  }

  onUsersList (cb) {
    this.addListener('INIT_USERS_LIST', cb)
  }

  onMessagesList (cb) {
    this.addListener('INIT_MESSAGES_LIST', cb)
  }

  saveSnapDictionary (cb) {
    this.addListener('RESPONSE_SNAP_DICT', cb)
  }

  onStartCall (cb) {
    this.addListener('START_CALL', cb)
  }

  onCloseCall (cb) {
    this.addListener('CLOSE_CALL', cb)
  }

  updateSnapDictionary (cb) {
    this.addListener('UPDATE_SNAP_DICT', cb)
  }

  onUserConnected (cb) {
    this.addListener('USER_CONNECTED', cb)
  }

  onUserDisconnected (cb) {
    this.addListener('USER_DISCONNECTED', cb)
  }

  onChangeAvatar (cb) {
    this.addListener('AVATAR_CHANGED', cb)
  }


  //--------------------- Invocare il server dal client ---------------------
  onChangeAvailability (availability) {
    this.socket.emit('CHANGED_AVAILABILITY', this.getUsername(), availability)
  }

  requestSnap () {
    this.socket.emit('REQUEST_SNAP_DICT')
  }

  broadcastMessage (message){
    this.socket.emit("NEW_MESSAGE", message)
  }

  sendPrivateMessage (message, interlocutor){
    console.log("passo", message, interlocutor);
    this._peers[interlocutor].send(message)
  }

  broadcastAvatarChanged (username, newAvatar){
    this.socket.emit("AVATAR_CHANGED", username, newAvatar)
  }

  disconnect () {
    this.socket.emit('DISCONNECT', this.getUsername())
    this.removeAllListeners() //di EventEmitter
    delete window.chatProxyInstance
  }

  connectToServer (username, hasWebcam) {
    this.socket = io()
    this.peer = new Peer(username, {
      host: location.hostname,
      port: 9000,
      path: '/chat'
    })
    this.setUsername(username)
    this.socket.emit('SEND_USERNAME', username, hasWebcam)

    /*
    Il polling di tentativo di riconnessione è automatico per l'oggetto socket.io-client.
    Quando il tentativo di riconnessione (evento "reconnecting") ha successo, il peer
    solleva l'evento 'SEND_USERNAME', in modo da reinizializzare le strutture del server.
    */
    /*this.socket.on('connect', () => {
    this.socket.emit('SEND_USERNAME', username)
    })*/

    //---------------- Ascolto degli eventi lanciati dal server ----------------
    this.socket.on('SEND_USERS_LIST', (list) => {
      this.emit('INIT_USERS_LIST', list)
    })

    this.socket.on('SEND_MESSAGES_LIST', (list) => {
      this.emit('INIT_MESSAGES_LIST', list)
    })

    this.socket.on('USER_CONNECTED', (newUser, userInfo) => {
      this._connectTo(newUser)
      this.emit('USER_CONNECTED', newUser, userInfo)
    })

    this.socket.on('RESPONSE_SNAP_DICT', (snapDictionary) => {
      this.emit('RESPONSE_SNAP_DICT', snapDictionary)
    })

    this.socket.on('UPDATE_SNAP_DICT', (username, params) => {
      this.emit('UPDATE_SNAP_DICT', username, params)
    })

    this.socket.on('USER_DISCONNECTED', (username, userInfo) => {
      this._disconnectFrom(username)
      this.emit('USER_DISCONNECTED', username, userInfo)
    })

    this.socket.on('NEW_MESSAGE', (message) => {
      this.emit('NEW_MESSAGE', message)
    })

    this.socket.on('AVATAR_CHANGED', (username, newAvatar) => {
      this.emit('AVATAR_CHANGED', username, newAvatar)
    })

    //---------------------------------------------------------------------------


    //------------------------ Ascolto eventi peer to peer ---------------------
    this.peer.on('connection', (conn) => {
      /*
      L'evento connection insorge nel momento in cui un peer invoca connect
      con l'id di 'questo' peer
      */
      this._registerPeer(conn.peer, conn)
    })
    //---------------------------------------------------------------------------

  }

  //-----------------------------------------------------------------------------

  registerCallListeners (ownStream) {
    let self = this
    if(ownStream){
      this.peer.on('call', (call) => {
        call.answer(ownStream)
        call.on('stream', (stream) => {
          self.emit('START_CALL', call.peer, stream)
        })
        call.on('close', () => {
          self.emit('CLOSE_CALL', call.peer)
        })
        call.on('err', () => {
          console.log('peer-to-peer: error call')
        })
      })
    }
  }

  requestStartCall (interlocutor, streamObj){
    let self = this
    let call = this.peer.call(interlocutor, streamObj)
    if(call){
      call.on('stream', (stream) => {
        self.emit('START_CALL', interlocutor, stream)
      })
      call.on('close', () => {
        self.emit('CLOSE_CALL', call.peer)
      })
      call.on('err', () => {
        console.log('peer-to-peer: error call')
      })
    }
    else{
      console.error("call obj is null");
    }
  }

  requestCloseCall (interlocutor){
    let openMediaConnection = this._peers[interlocutor].provider.connections[interlocutor][1]

    if(openMediaConnection.open){
      openMediaConnection.close()
    }
  }

  _connectTo (newUser) {
    let conn = this.peer.connect(newUser)
    conn.on('open', () => {
      this._registerPeer(newUser, conn)
    })
  }

  _registerPeer (username, conn) {
    console.log('Registering', username)
    let self = this
    this._peers[username] = conn

    conn.on('data', (message) => {
      self.emit("NEW_PRIVATE_MESSAGE", message, conn.peer)
    })
  }

  takeSnapshot (immediateSnapUpdate, replaceSnapCase){
    const camRef = this.getCamRef()

    const myWidth = camRef.width
    const myHeight = camRef.height

    const myCanvas = document.createElement('canvas')
    myCanvas.width = myWidth
    myCanvas.height = myHeight

    const context = myCanvas.getContext('2d')
    context.drawImage(camRef, 0, 0, myWidth, myHeight)
    let snap = myCanvas.toDataURL('image/png')

    this.prepareEntryDictionary(snap, immediateSnapUpdate, replaceSnapCase)
    return true
  }

  prepareEntryDictionary (snap, immediateSnapUpdate, replaceSnapCase) {
    const entryDictionary = {
      username : this.getUsername(),
      snap
    }

    this.socket.emit("SEND_SNAP", entryDictionary, immediateSnapUpdate, replaceSnapCase)
  }

  _disconnectFrom (username) {
    this.requestCloseCall(username)

    this._peers[username].close()
    delete this._peers[username]
  }
  //----------------------------------------------------------------------------
}


export default getChatProxyInstance
