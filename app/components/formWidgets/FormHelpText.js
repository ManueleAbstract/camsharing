import classNames from 'classnames'
import React, {Component} from 'react'
import FontAwesome from 'react-fontawesome'
import {
  Popover,
  Overlay
} from 'react-bootstrap'

export default class FormHelpText extends Component  {

  render () {
    const {status, children, type, customStyle} = this.props
    let cssClass = classNames({
        'formtooltip': true,
        'error': status == 'error',
        'success': status == 'success',
        'up': type == 'up',
        'right': type == 'right'
    })

    let icon = 'question-circle-o'
    switch (status) {
      case 'error':
        icon = 'exclamation-circle'
        break;
      case 'success':
        icon = 'check-circle-o'
        break;
    }
    return (
      <div>
        <Overlay
          trigger="focus"
          show={true}
          target={this.props.elemRef}
          placement="right"
        >
          <Popover id="popover-contained">
            {children}
          </Popover>
        </Overlay>
      </div>
    )
  }
}
