import React, { Component } from 'react'
import { Col } from 'react-bootstrap'
import FormHelpText from './FormHelpText'


export default class FormTooltip extends Component {

  constructor (props) {
    super(props)
    this.renderTooltip = this.renderTooltip.bind(this)
  }

  renderTooltip() {
    let message = this.props.help
    let status = 'info'

    if (! this.props.hideSuccessStatus && this.props.showSuccess()) {
      status = 'success'
      message = ' '
    } else {
      if (this.props.showErrors()) {
        const errorMessages = this.props.getErrorMessages()
        status = 'error'
        if (errorMessages.length > 0) {
          message = errorMessages.map((msg, key) => (
            <span key={key}>{msg}</span>
          ))
        }
      }
    }
    if (message)
      return (
        <FormHelpText
            elemRef={this.props.elemRef}
            customStyle={this.props.customStyle}
            type={this.props.fullwidth? 'up' : 'right' }
            status={status}
         >
            {message}
         </FormHelpText>
       )
  }

  render () {
    let tooltip = null
    const tooltip_inner = this.renderTooltip()
    if (tooltip_inner) {
      tooltip = (
        <Col md={this.props.n_cols}
             className='helptext-wrapper'>{tooltip_inner}</Col>
      )
    }
    return tooltip
  }
}
