import React from 'react'
import InputPassword from 'react-ux-password-field'
import { Input } from '.'

class PasswordInput extends Input {

  constructor(props){
    super(props)
    this.renderElementCustom = this.renderElementCustom.bind(this)

    this.renderElement = this.renderElementCustom
  }

  customChangeValue(value) {
    var value = value.trim()
    if(this.props.preventSpaces){
      let keyAcceptability = this.preventSpaces(value)
      if(keyAcceptability){
        this.setValue(value)
      }
    }
    else {
      this.setValue(value)
    }
  }

  renderElementCustom () {
    const STRENGTH_LANG = ['Bad', 'Not good', 'Decent', 'Strong', 'Great']
    const asyncZXCVBN = 'https://cdnjs.cloudflare.com/ajax/libs/zxcvbn/1.0/zxcvbn.min.js'

    return (
      <InputPassword
        placeholder="Password*"
        className="form-control"
        minScore={2}
        name={this.props.name}
        strengthLang={STRENGTH_LANG}
        ref="inputPassword"
        id="inputPassword"
        onChange={this.customChangeValue}
        value={this.getValue()}
        zxcvbn={asyncZXCVBN}
        required
      />
    )
  }
}

export default PasswordInput
