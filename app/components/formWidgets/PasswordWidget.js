import React, { Component } from 'react'
import {
  Input,
  PasswordInput
}
from './'

export default class PasswordWidget extends Component {

  renderPasswordInput() {
    return (
      <PasswordInput
        name={this.props.name}
        label="Password"
        validations={{
          minLength: 6,
          maxLength: 50
        }}
        validationErrors={{
          minLength: 'Please, type at least 6 characters.',
          maxLength: 'Too long string, max 50 characters.'
        }}
        fullwidth
        preventSpaces
        required
      />
    )
  }

  renderRetypePasswordInput () {
    return (
      <Input
        type='password'
        label='Confirm Password'
        name='passwordConfirm'
        placeholder='Repeat password*'
        validations='equalsField:password'
        validationErrors={{
          equalsField: 'Passwords do not match.'
        }}
        fullwidth
        required
      />
    )
  }

  render () {
    let password_wdgt = this.renderPasswordInput()
    let retype_password_wdgt = this.renderRetypePasswordInput()

    return (
      <div>
        {password_wdgt}
        {retype_password_wdgt}
      </div>
    )
  }

}
