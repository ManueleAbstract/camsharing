import classNames from 'classnames'
import React from 'react'
import FormTooltip from './FormTooltip'

export const showSuccess = (el) => {
  return (el.isPristine() && el.isValid())
}

export const getExtraRowProps = (el) => {
  let row_props = el.getRowProperties()
  if (el.showSuccess()) {
    row_props.rowClassName = `success ${row_props.rowClassName}`
  }
  return row_props
}

export const getNCols = (el) => {
  let cols = { right: 6, left: 6 }
  if (el.props.fullwidth) {
    cols.right = 12
    cols.left = 12
  } else if (el.props.columnWidth) {
    cols.left = el.props.columnWidth
    cols.right = 12 - el.props.columnWidth
    if (! cols.right || cols.right < 1 )
      cols.right = 0
  } else if (el.props.hideTooltip) {
    cols.right = 0
    cols.left = 12
  }
  return cols
}


export const getTooltip = (el, n_cols) => {
  if (el.props.hideTooltip)
    return null

  return (
    <FormTooltip
      elemRef={el}
      help={el.props.help}
      hideSuccessStatus={el.props.hideSuccessStatus}
      showSuccess={el.showSuccess}
      showErrors={el.showErrors}
      getErrorMessages={el.getErrorMessages}
      fullwidth={el.props.fullwidth}
      n_cols={n_cols.right} />
  )
}


export const getRowClass = (el) => {
  return classNames({
     [`${el.props.type}-widget`]: el.props.type,
     [`${el.props.name}-widget`]: el.props.name,
     'widget-row': true
  })
}
