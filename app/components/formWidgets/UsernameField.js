import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  Input,
  Field
} from './'
import {
  checkAccountAvailable
} from '../../actions'

class UsernameField extends Component {

  constructor(props){
    super(props)

    this.checkAccountAvailable = this.checkAccountAvailable.bind(this)
  }

  wrapField = (element, help='') => (
    <Field help={help}>{element}</Field>
  )

  checkAccountAvailable(value){
    if(value){
      this.props.checkAccountAvailable(value, "accountAvailable")
    }
  }

  render(){
    let inputField = (
      <Input
        type="text"
        placeholder="Username*"
        validations={{
          isAlphanumeric:"isAlphanumeric",
          maxLength: 15
        }}
        validationErrors={{
          isAlphanumeric: 'String cannot contain special characters.',
          maxLength: 'Too long string, max 15 characters.'
        }}
        name={this.props.name}
        onChange={this.checkAccountAvailable}
        preventSpaces
        fullwidth
        required
      />
    )

    let toRender
    if(this.props.accountAvailable === false){
      toRender = this.wrapField(inputField, "Account unavailable!")
    }
    else{
      toRender = this.wrapField(inputField, "")
    }
    return (
      <div>
        {toRender}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  let ret = {}
  if(state.registration){
    ret = {
      accountAvailable: state.registration.accountAvailable
    }
  }
  return ret
}

const mapDispatchToProps = (dispatch) => {
  return {
    checkAccountAvailable: (username, storeFieldName) => {
      dispatch(checkAccountAvailable(username, storeFieldName))
    }
  }
}

UsernameField = connect(mapStateToProps, mapDispatchToProps)(UsernameField)

export default UsernameField
