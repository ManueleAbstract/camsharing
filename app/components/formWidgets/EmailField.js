import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  Input,
  Field
} from './'
import {
  checkAccountAvailable
} from '../../actions'

class EmailField extends Component{

  constructor(props){
    super(props)

    this.checkAccountAvailable = this.checkAccountAvailable.bind(this)
  }

  wrapField = (element, help='') => (
    <Field help={help}>{element}</Field>
  )

  checkAccountAvailable(value){
    if(value){
      this.props.checkAccountAvailable(value, "emailAvailable")
    }
  }

  render(){
    let inputField = (
      <Input
        type="email"
        placeholder="Email address*"
        name={this.props.name}
        validations={{
          isEmail:"isEmail",
          maxLength: 50
        }}
        validationErrors={{
          isEmail: 'String not email format.',
          maxLength: 'Too long string, max 50 characters.'
        }}
        onChange={this.checkAccountAvailable}
        value=""
        preventSpaces
        fullwidth
        required
      />
    )

    let toRender
    if(this.props.emailAvailable === false){
      toRender = this.wrapField(inputField, "Email already used!")
    }
    else{
      toRender = this.wrapField(inputField, "")
    }
    return (
      <div>
        {toRender}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  let ret = {}
  if(state.registration){
    ret = {
      emailAvailable: state.registration.emailAvailable
    }
  }
  return ret
}

const mapDispatchToProps = (dispatch) => {
  return {
    checkAccountAvailable: (email, storeFieldName) => {
      dispatch(checkAccountAvailable(email, storeFieldName))
    }
  }
}

EmailField = connect(mapStateToProps, mapDispatchToProps)(EmailField)

export default EmailField
