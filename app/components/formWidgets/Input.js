import React from 'react'
import {
  Input as BaseInput,
  Row as RowInput
} from 'formsy-react-components'
import { Col, Row } from 'react-bootstrap'
import { getTooltip, showSuccess, getExtraRowProps, getNCols, getRowClass } from './base'


export default class Input extends BaseInput  {
  constructor(props){
    super(props)
    this.preventSpaces = this.preventSpaces.bind(this)
    this.customChangeValue = this.customChangeValue.bind(this)
    this.customGetErrorMessages = this.customGetErrorMessages.bind(this)

    this.changeValue = this.customChangeValue
    this.originalErrorMessages = this.getErrorMessages
    this.getErrorMessages = this.customGetErrorMessages
  }

  showSuccess = () => {
    return showSuccess(this)
  }

  getExtraRowProps() {
    return getExtraRowProps(this)
  }

  getNCols = () => {
    return getNCols(this)
  }

  customGetErrorMessages(){
    let errorMsg = this.props.errorMsg
    if(errorMsg) {
      return [errorMsg]
    }
    else{
      return this.originalErrorMessages()
    }
  }

  getTooltip = (n_cols) => {
    let element = this.renderElement()
    if(element && element.props.value){
      return getTooltip(this, n_cols)
    }
  }

  getRowClass = () => {
    return getRowClass(this)
  }

  customChangeValue(event) {
    var value = event.currentTarget.value.trim()
    if(this.props.preventSpaces){
      let keyAcceptability = this.preventSpaces(value)
      if(keyAcceptability){
        this.setValue(value)
      }
    }
    else {
      this.setValue(value)
    }
    
    if(this.props.onChange){
      this.props.onChange(value)
    }
  }

  preventSpaces(typed) {
    let check = /\s/.test(typed)
    return !check
  }

  render () {
    let element = this.renderElement()
    const layout = this.getLayout()
    const n_cols = this.getNCols()

    if (this.props.type === 'hidden') {
      return element
    }

    if (layout === 'elementOnly') {
      return element
    }

    if (this.props.addonBefore || this.props.addonAfter || this.props.buttonBefore || this.props.buttonAfter) {
      element = this.renderInputGroup(element);
    }
    return (
      <Row className={this.getRowClass()}>
        <Col md={n_cols.left}>
          <RowInput
              {...this.getRowProperties()}
              htmlFor={this.getId()} >
              {element}
          </RowInput>
        </Col>
        {this.getTooltip(n_cols)}
      </Row>
    )
  }
}
