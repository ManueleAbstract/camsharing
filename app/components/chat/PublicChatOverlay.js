import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import {
  MessageInput,
  MessagesList
} from '../../components/chat'
import {
  Panel
} from 'react-bootstrap'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import IconButton from 'material-ui/IconButton'
import Badge from 'material-ui/Badge'
import ActionSpeakerNotes from 'material-ui/svg-icons/action/speaker-notes'
import ContentClear from 'material-ui/svg-icons/content/clear'
import {fullWhite} from 'material-ui/styles/colors'

class PublicChatOverlay extends Component {

  constructor(props){
    super(props)
    this.state = {
      show: false,
      unreadMessages: 0
    }

    this.onMessage = this.onMessage.bind(this)
    this.addMessage = this.addMessage.bind(this)
    this.sendMessage = this.sendMessage.bind(this)
    this.handleOpen = this.handleOpen.bind(this)
    this.handleClose = this.handleClose.bind(this)

    const chatProxy = this.props.chatProxy
    chatProxy.onMessage(this.onMessage)
  }

  handleOpen(){
    this.setState({
      show: true,
      unreadMessages: 0
    })
  }

  handleClose(){
    this.setState({show: false})
  }

  sendMessage(text) {
    const message = {
      content: text,
      author: this.props.username
    }

    //Invia il messaggio agli altri
    this.props.chatProxy.broadcastMessage(message)

    //Visualizza il messaggio nella chat locale
    this.refs.messagesList.addMessage(message)
  }

  onMessage(message){
    if(!this.state.show){
      this.setState({
        unreadMessages: this.state.unreadMessages + 1
      })
    }
    this.addMessage(message)
  }

  addMessage(message){
    this.refs.messagesList.addMessage(message)
  }

  render(){
    const buttonStyle = {
      margin: 0,
      padding: 0
    }

    const badgeStyle = {
      margin: 0,
      padding: 0,
      bottom: 12,
      top: "noTop",
      left: "65%",
      backgroundColor: "#FF5C5C"
    }

    const openButton = (<FlatButton
                          onTouchTap={this.handleOpen}
                          backgroundColor="#33B5E5"
                          hoverColor="#178ab4"
                          icon={<ActionSpeakerNotes color={fullWhite} />}
                          style={buttonStyle}
                        />)

    const wrapButtonWithBadge = () => (
      <Badge
        badgeContent={this.state.unreadMessages}
        secondary={true}
        badgeStyle={badgeStyle}
      >
        {openButton}
      </Badge>
    )

    const openButtonToRender = () => {
      let newButton
      if(this.state.unreadMessages > 0){
        newButton = wrapButtonWithBadge()
      }
      else{
        newButton = openButton
      }
      return newButton
    }

    let contentStyle = {
      top: "0%",
      width: "100%",
      maxWidth: "95%",
      padding: "0"
    }

    let dialogStyle
    if(!this.state.show) {
      dialogStyle = {
        display: "none"
      }
    }

    //bodyStyle={{overflowY: "scroll"}}

    const closeButtonStyle = {
      position: "fixed",
      left:"95%",
      zIndex: "1000",
      top: "-.5%",
      left: "95.5%"
    }

    return (
      <div>
        {openButtonToRender()}
        <Dialog
          open={true}
          onRequestClose={this.handleClose}
          style={dialogStyle}
          contentStyle={contentStyle}
          bodyStyle={{padding: "0", overflowY: "hidden", marginBottom: "55px" }}
        >
          <div className="messages-list-container">
            <IconButton
              onClick={this.handleClose}
              style={closeButtonStyle}
            >
              <ContentClear />
            </IconButton>
            <div className='contenitore'>
              <MessagesList
                ref="messagesList"
                chatProxy={this.props.chatProxy}
                username={this.props.username}
              />
            </div>
          </div>
          <MessageInput
            sendMessage={this.sendMessage}
            handleClose={this.handleClose}
          />
        </Dialog>
      </div>
    )
  }
}

export default PublicChatOverlay
