import React, { Component } from 'react'

class CallOverlay extends Component {

  componentWillMount(){
    this.handleClose = this.handleClose.bind(this)
    this.setFooterControls = this.setFooterControls.bind(this)
  }

  handleClose(){
  }

  setFooterControls(){

  }

  render() {
    let videoTag
    let videoStyle = {
      width: this.props.width,
      height: this.props.height
    }

    let interlocutor_stream = this.props.interlocutorObj.stream
    if(interlocutor_stream){
      window.URL = window.URL || window.webkitURL || window.mozURL || window.msURL
      const src = window.URL.createObjectURL(interlocutor_stream)

      //muted
      videoTag = (
        <video
          autoPlay
          src={src}
          onCanPlayThrough={this.setFooterControls}
          style={videoStyle}
        />
      )
    }

    return (
      <div>
        { videoTag }
        <div className="box-username">
          <span>{this.props.username}</span>
        </div>
      </div>
    )
  }
}

export default CallOverlay
