import React, { Component } from 'react'
import {
  MenuOptions
 } from './'
import {
  AvatarMenu
} from '../../containers/chat'
 import { SettingsPanel } from '../../containers/settings'

class Menu extends Component{

  constructor(props){
    super(props)
    this.state = {
      openedSettings: false
    }

    this.toggleSettingsPanel = this.toggleSettingsPanel.bind(this)
    this.handleClose = this.handleClose.bind(this)
  }

  toggleSettingsPanel(){
    this.setState({openedSettings: !this.state.openedSettings})
  }

  handleClose(){
    this.setState({openedSettings: false})
  }

  render(){
    const sizeAvatarMenu = 48

    const menuButton = (<AvatarMenu
                          username={this.props.username}
                          size={sizeAvatarMenu}
                        />)
    return(
      <div>
        <MenuOptions
          size={sizeAvatarMenu}
          menuButton={menuButton}
          logout={this.props.logout}
          changeAvailability={this.props.changeAvailability}
          toggleSettingsPanel={this.toggleSettingsPanel}
        />
        <SettingsPanel
          openedSettings={this.state.openedSettings}
          handleClose={this.handleClose}
          username={this.props.username}
          chatProxy={this.props.chatProxy}
        />
      </div>
    )
  }
}

export default Menu
