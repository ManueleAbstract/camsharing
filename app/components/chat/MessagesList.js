import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { ChatMessage } from './'
import moment from 'moment'

class MessagesList extends React.Component{

  constructor(props){
    super(props)
    this.state = {
      messages: []
    }

    this.addMessage = this.addMessage.bind(this)
    this.initMessagesList = this.initMessagesList.bind(this)

    if(this.props.chatProxy){
      this.props.chatProxy.onMessagesList(this.initMessagesList)
    }
  }

  initMessagesList(messagesList){
    this.setState({
      messages: messagesList
    })
  }

  addMessage(message) {
    if(!message.time){
      //Caso in cui l'utente visualizza il suo stesso messaggio
      message.time = (+moment()).toString()
    }
    var messages = this.state.messages
    var container = ReactDOM.findDOMNode(this.refs.messageContainer)
    messages.push(message)
    this.setState({ messages: messages })
    // Smart scrolling - when the user is
    // scrolled a little we don't want to return him back

    if (container.scrollHeight -
        (container.scrollTop + container.offsetHeight) >= 50) {
      this.scrolled = true
    } else {
      this.scrolled = false
    }
  }

  componentDidUpdate() {
    if (this.scrolled) {
      return
    }
    var container = ReactDOM.findDOMNode(this.refs.messageContainer)
    container.scrollTop = container.scrollHeight
  }

  render() {
    let username = this.props.username
    let messages = this.state.messages
    let toRender
    if (!messages.length) {
      toRender = <div className="chat-no-messages">No messages</div>
    }
    else{
      toRender = messages.map(function (m) {
        return (
          <ChatMessage
            message={m}
            key={m.time}
            username={username}
          />
        )
      })
    }
    return (
      <div className="messagesList" ref="messageContainer">
        {toRender}
      </div>
    )
  }
}

export default MessagesList
