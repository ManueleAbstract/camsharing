import React, { Component } from 'react'
import ImagePhotoCamera from 'material-ui/svg-icons/image/photo-camera'
import IconButton from 'material-ui/IconButton'

class SnapCommandsWrapper extends Component{
  constructor(props){
    super(props)

    this.state = {
      snapshotReplaceable: true
    }

    this.replaceSnapshot = this.replaceSnapshot.bind(this)
  }

  componentWillUpdate(nextProps, nextState){
    if(!nextState.snapshotReplaceable){
      setTimeout(() => {
        this.setState({
          snapshotReplaceable: true
        })
      }, 3000)
    }
  }

  replaceSnapshot(){
    let immediateSnapUpdate = true
    let replaceSnapshotCase = true
    this.props.replaceSnapshot(immediateSnapUpdate, replaceSnapshotCase)
    this.setState({
      snapshotReplaceable: false
    })
  }

  render(){
    let replaceSnapshotStyle = {
      position: "absolute",
      height: "noHeight",
      width: "noWidth",
      zIndex: "20",
      padding: "4px"
    }

    return(
      <div>
        <IconButton
          tooltip="Replace snapshot"
          tooltipPosition="bottom-right"
          style={replaceSnapshotStyle}
          disabled={!this.state.snapshotReplaceable}
          onClick={this.replaceSnapshot}
        >
          <ImagePhotoCamera color="hsla(151, 100%, 45%, 0.59)"/>
        </IconButton>
        {this.props.children}
      </div>
    )
  }
}

export default SnapCommandsWrapper
