import React, {Component} from 'react'
import ReactTooltip from 'react-tooltip'
import moment from 'moment'
import Chip from 'material-ui/Chip'
import { momentToDate } from '../../libs/timingUtils'

class ChatMessage extends Component{

  render() {
    let msg = this.props.message;
    let msgTime = moment(Number(msg.time))
    let completeDate = momentToDate(msgTime)

    let username = this.props.username
    let author = msg.author
    let chipStyle = {
      margin: "4"
    }
    if(author === username){
      chipStyle = {
        ...chipStyle,
        backgroundColor: "#33B5E5"
      }
      author = "you"
    }
    return (
      <div className="chat-message">
        <Chip style={chipStyle}>
          <div style={{display:"inline-block"}}>
            <div data-tip data-for={msgTime.toString()}>
              <span className={"message-author"}>{author}: </span>
              <span className={"message-content"}>{msg.content}</span>
            </div>
            <ReactTooltip
              id={msgTime.toString()}
              place="right"
              effect='solid'
              class='chat-tooltip'
            >
              {completeDate}
            </ReactTooltip>
          </div>
        </Chip>
      </div>
    )
  }
}

export default ChatMessage
