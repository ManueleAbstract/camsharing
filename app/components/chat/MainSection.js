import React from 'react'
import ReactDOM from 'react-dom'
import {
  Row as RowBoot,
  Grid as GridBoot,
  Col as ColBoot
} from 'react-bootstrap'
import {
  Grid as GridFlex
} from 'react-flexbox-grid'
import {
  CamStream,
  UsersList
} from '../../containers/chat'
import RefreshIndicator from 'material-ui/RefreshIndicator'
import Dialog from 'material-ui/Dialog'
import NotificationSystem from 'react-notification-system'

class MainSection extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      loading: true
    }

    this.clearInterval = this.clearInterval.bind(this)
    this.userConnected = this.userConnected.bind(this)
    this.userDisconnected = this.userDisconnected.bind(this)
    this.showNotification = this.showNotification.bind(this)

    //binding chatProxy listeners & chatPage callbacks
    const chatProxy = this.props.chatProxy
    chatProxy.onUserConnected(this.userConnected)
    chatProxy.onUserDisconnected(this.userDisconnected)
  }

  componentWillMount(){
    this.interval = setInterval(()=>{
      this.usersListRef = document.getElementById('usersListRef')
      this.camStreamRef = document.getElementById('camStreamRef')
      if(this.usersListRef && this.camStreamRef){
        this.clearInterval()
      }
    }, 100)
  }

  userConnected(username){
    this.showNotification(username, "CONNECTED")
  }

  userDisconnected(username){
    this.showNotification(username, "DISCONNECTED")
  }

  showNotification(username, type) {
    let level, title, children, uid

    switch (type) {
      case "CONNECTED":
        children = (
          <div>
            <p>User <span className='bold-text'>{username}</span> logged in</p>
          </div>
        )
        title = 'Connection'
        level = "success"
        break
      case "DISCONNECTED":
        children = (
          <div>
            <p>User <span className='bold-text'>{username}</span> logged out</p>
          </div>
        )
        title = 'Disconnection'
        level = "error"
        break
      case "WELCOME":
        children = (
          <div>
            <p>Hi <span className='bold-text'>{username}</span>!</p>
          </div>
        )
        title = "Welcome"
        level = "info"
        uid = "welcome_user"
        break
      default:
        level = "info"
    }
    this.notifAppearsChat.addNotification({
      position: 'br',
      title,
      level,
      children,
      uid
    })
  }

  clearInterval(){
    clearInterval(this.interval)
    this.setState({
      loading: false
    })
  }

  render(){
    const loaderStyle = {
      display: 'inline-block',
      position: "fixed",
      top: "50%",
      left: "50%",
      transform: "translate(-50%, -50%)"
    }

    let loader
    let colUsersStyle

    if(this.state.loading){
      loader = (<Dialog
                  modal={true}
                  open={true}
                  bodyStyle={{padding:"0px"}}
                  contentStyle={{transition:"noTransition"}}
                >
                  <RefreshIndicator
                    size={40}
                    left={10}
                    top={0}
                    status="loading"
                    style={loaderStyle}
                  />
                </Dialog>)
    }
    else{
      colUsersStyle = "users"
      this.showNotification(this.props.username, "WELCOME")
    }

    return(
      <section className="mainSection">
        <NotificationSystem ref={(ref) => this.notifAppearsChat = ref} />
        {loader}
        <GridBoot>
          <RowBoot>
            <ColBoot md={8} className="snapshotsColumn">
              <GridFlex>
                <CamStream {...this.props} />
              </GridFlex>
            </ColBoot>
            <ColBoot md={4} className={colUsersStyle}>
              <UsersList
                username={this.props.username}
                chatProxy={this.props.chatProxy}
              />
            </ColBoot>
          </RowBoot>
        </GridBoot>
      </section>
    )
  }
}

export default MainSection
