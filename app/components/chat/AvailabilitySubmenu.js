import React from 'react'
import MenuItem from 'material-ui/MenuItem'
import Divider from 'material-ui/Divider'
import ArrowDropRight from 'material-ui/svg-icons/navigation-arrow-drop-right'
import ImageBrightness1 from 'material-ui/svg-icons/image/brightness-1'
import {greenA400} from 'material-ui/styles/colors'
import {redA700} from 'material-ui/styles/colors'

class AvailabilitySubmenu extends React.Component {
  componentWillMount(){
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick(availability){
    this.props.changeAvailability(availability)
    this.props.handleRequestClose()
  }

  render(){
    return(
      <div>
        <MenuItem
          rightIcon={<ImageBrightness1 color={greenA400}/>}
          primaryText="Available"
          onClick={this.handleClick.bind(this, 'available')}
        />
        <Divider />
        <MenuItem
          rightIcon={<ImageBrightness1 color={redA700}/>}
          primaryText="Unavailable"
          onClick={this.handleClick.bind(this, 'unavailable')}
        />
      </div>
    )
  }
}

export default AvailabilitySubmenu
