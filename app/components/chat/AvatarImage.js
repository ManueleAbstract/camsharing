import React from 'react'
import ReactDOM from 'react-dom'
import defaultAvatar from '../../../static/offline_user.png'

class AvatarImage extends React.Component {

  render() {
    let src = this.props.src
    let avatarImage

    if(src){
      avatarImage = src
    }
    else{
      avatarImage = defaultAvatar
    }

    let {
      borderRadius='100%',
      size,
      username
    } = this.props

    size = size+"px"

    const imageStyle = {
      display: 'block',
      borderRadius
    }

    let innerStyle = {
      lineHeight: size,
      textAlign: 'center',
      borderRadius
    }

    const inheritedStyle = this.props.style

    innerStyle = {
      ...inheritedStyle,
      ...innerStyle
    }

    if (size) {
      imageStyle.width = innerStyle.width = size
      imageStyle.height = innerStyle.height = size
    }

    return (
      <div style={innerStyle}>
        <img className="avatar-image" style={imageStyle} src={avatarImage} />
        {this.props.children}
      </div>
    )
  }
}

export default AvatarImage
