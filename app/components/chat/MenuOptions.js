import React from 'react'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'
import Divider from 'material-ui/Divider'
import Popover from 'material-ui/Popover'
import ActionExitToApp from 'material-ui/svg-icons/action/exit-to-app'
import ActionSettings from 'material-ui/svg-icons/action/settings'
import ArrowDropRight from 'material-ui/svg-icons/navigation-arrow-drop-right'
import { SnapTimerSubmenu } from '../../containers/chat'
import { AvailabilitySubmenu } from './'
import FlatButton from 'material-ui/FlatButton'
import FloatingActionButton from 'material-ui/FloatingActionButton'

class MenuOptions extends React.Component {
  constructor(props, context) {
    super(props, context)

    this.handleRequestClose = this.handleRequestClose.bind(this)
    this.handleRequestOpen = this.handleRequestOpen.bind(this)
    this.toggleSettingsPanel = this.toggleSettingsPanel.bind(this)

    this.state = {
      open: false
    }
  }

  handleRequestClose() {
    this.setState({
      open: false
    })
  }

  handleRequestOpen(e){
    e.preventDefault()

    this.setState({
      open: true,
      anchorEl: e.currentTarget
    })
  }

  toggleSettingsPanel(){
    this.props.toggleSettingsPanel()
    this.handleRequestClose()
  }

  //vedere icone https://design.google.com/icons
  render() {
    const size = this.props.size
    const myStyle = {
      height: size,
      lineHeight: size+'px',
      minWidth: size+'px',
      borderRadius: "50%"
    }

    const iconStyle = {
      width: "48px",
      height: "48px",
      backgroundColor: "transparent"
    }

    return(
      <div>
        <div>
          <FloatingActionButton
            onClick={this.handleRequestOpen}
            style={myStyle}
            iconStyle={iconStyle}
          >
            {this.props.menuButton}
          </FloatingActionButton>
          <Popover
            open={this.state.open}
            anchorEl={this.state.anchorEl}
            onRequestClose={this.handleRequestClose}
            canAutoPosition={false}
          >
            <Menu className="avatar-menu">
              <AvailabilitySubmenu
                changeAvailability={this.props.changeAvailability}
                handleRequestClose={this.handleRequestClose}
              />
              <Divider />
              <SnapTimerSubmenu
                handleRequestClose={this.handleRequestClose}
              />
              <Divider />
              <MenuItem
                primaryText="Settings"
                leftIcon={<ActionSettings />}
                onClick={this.toggleSettingsPanel}
              />
              <Divider />
              <MenuItem
                primaryText="Logout"
                leftIcon={<ActionExitToApp />}
                onClick={this.props.logout}
              />
            </Menu>
          </Popover>
        </div>
      </div>
    )
  }
}

export default MenuOptions
