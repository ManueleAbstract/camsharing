import React from 'react'
import { Panel } from 'react-bootstrap'

class RegistrationSubmitted extends React.Component{

  componentWillMount(){
    if(window.onbeforeunload){
      window.onbeforeunload = undefined
    }
  }

  render(){
    return(
      <div>
        <div className="container advice">
          <Panel>
            <h1>
              Subscription request correctly submitted!
            </h1>
            <h3>
              Please check your email to activate the created account
              (possibly check also in the spam category).
            </h3>
          </Panel>
        </div>
      </div>
    )
  }
}

export default RegistrationSubmitted
