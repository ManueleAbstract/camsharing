import React, { Component } from 'react'
import ReactDom from 'react-dom'
import { connect } from 'react-redux'
import AvatarCropper from 'react-avatar-cropper'
import defaultAvatar from '../../../static/offline_user.png'
import { downScaleImage } from '../../libs/resizingImgUtils'
import { Well } from 'react-bootstrap'

var FileUpload = React.createClass({

  handleFile: function(e) {
    var reader = new FileReader()
    var file = e.target.files[0]

    if (!file) return

    reader.onload = function(img) {
      ReactDom.findDOMNode(this.refs.in).value = ''

      this.props.handleFileChange(img.target.result)
    }.bind(this)
    reader.readAsDataURL(file)
  },


  render: function() {
    return (
      <input ref="in" type="file" accept="image/*" onChange={this.handleFile} />
    )
  }
})

class MyCropper extends Component {
  constructor(props){
    super(props)

    let currentAvatar = this.props.currentAvatar

    this.state = {
      cropperOpen: false,
      img: null,
      croppedImg: defaultAvatar
    }

    this.handleFileChange = this.handleFileChange.bind(this)
    this.handleRequestHide = this.handleRequestHide.bind(this)
    this.handleCrop = this.handleCrop.bind(this)
    this.resizeImage = this.resizeImage.bind(this)
  }

  resizeImage(dataURI, scale){
    //Ridimensionamento senza perdita di qualità

    let imgTag = document.createElement('img')
    imgTag.src = dataURI

    return downScaleImage(imgTag, scale)
  }

  handleFileChange(dataURI){
    this.setState({
      img: dataURI,
      croppedImg: this.state.croppedImg,
      cropperOpen: true
    })
  }

  handleCrop(dataURI){
    let scale = 0.12 //l'immagine risultante sarà di 50x50 px
    let resizedImg = this.resizeImage(dataURI, scale)

    this.setState({
      cropperOpen: false,
      croppedImg: resizedImg,
      img: null
    })
    this.props.setAvatarImage(resizedImg)
  }

  handleRequestHide(){
    this.setState({
      cropperOpen: false
    })
  }

  render(){
    let wellStyle = {
      margin: "10px 0px",
      backgroundColor: "#FFFFFF",
      padding: "2px",
      maxWidth: "56px",
      borderRadius: "100%"
    }

    let currentAvatar = this.props.currentAvatar
    let uploadedAvatar = this.state.croppedImg
    let imageToRender
    if(this.props.currentAvatar){
      imageToRender = currentAvatar
    }
    else{
      if(uploadedAvatar){
        imageToRender = uploadedAvatar
      }
    }

    let avatar
    if(this.state.cropperOpen){
      avatar = (<AvatarCropper
                  onRequestHide={this.handleRequestHide}
                  cropperOpen={this.state.cropperOpen}
                  onCrop={this.handleCrop}
                  image={this.state.img}
                  width={400}
                  height={400}
                />)
    }
    return (
      <div>
        <div className="avatar-photo">
          <FileUpload handleFileChange={this.handleFileChange} />
          <Well style={wellStyle}>
            <img height={50} width={50} src={imageToRender} style={{borderRadius: "100%"}}/>
          </Well>
        </div>
        {avatar}
      </div>
    )
  }
}

export default MyCropper
