const registration = (state=null, action) => {
  switch(action.type){
    case "RESET_STORE":
      return null

    case 'REGISTRATION_REQUEST':
      console.log('registration save request submitted')
      return state

    case 'REGISTRATION_RESULT':
      return {
        ...state,
        username: action.payload.username,
        status: action.payload.status
      }

    case 'REGISTRATION_ERROR':
      console.log('registration save request failed')
      return state

    case 'ACCOUNT_CHECK_REQUEST':
      console.log('check username request submitted')
      return state

    case 'ACCOUNT_CHECK_RESULT':
      let storeFieldName = action.payload.storeFieldName
      let ret = {
        ...state
      }
      ret[storeFieldName] = action.payload.accountAvailable
      return ret

    case 'ACCOUNT_CHECK_ERROR':
      console.log('check username request error')
      return state

    case 'CONFIRMATION_REQUEST':
      console.log('registraton confirmation request submitted')
      return state

    case 'CONFIRMATION_RESULT':
      return {
        ...state,
        confirmed: action.payload.confirmed
      }

    case 'CONFIRMATION_ERROR':
      console.log('registraton confirmation request failed')
      return state

    default:
      return state
  }
}

export default registration
