const auth = (state=null, action) => {
  switch (action.type) {
    case "RESET_STORE":
      return null

    case 'LOGIN_RESULT':
      let username = action.payload.username
      if(username){
        localStorage.setItem('username', username)
      }
      return {
        userFound: action.payload.userFound
      }

    case 'LOGIN_ERROR':
      console.log('richiesta login fallita');
      return null

    case 'LOGIN_REQUEST':
      console.log('richiesta login');
      return null

    default:
      return state
  }
}

export default auth
