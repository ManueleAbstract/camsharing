import _ from "lodash"

const call = (state=null, action) => {
  let username
  let currentCalls

  switch (action.type) {
    case "RESET_STORE":
      return null

    case 'SET_CALL':
      currentCalls = (state && state.call && state.call.currentCalls) ? state.call.currentCalls : {}
      username = action.interlocutorObj.name
      currentCalls[username] = _.cloneDeep(action.interlocutorObj)

      return {
        ...state,
        currentCalls: {...currentCalls}
      }

    case 'CLOSE_CALL':
      username = action.username
      let ret

      if(state &&
         state.currentCalls &&
         (!!Object.keys(state.currentCalls)) &&
         state.currentCalls[username]
       ){
        currentCalls = state.currentCalls
        console.log("entro in close_call ", currentCalls );
        delete currentCalls[username]

        ret = {
          ...state,
          currentCalls: {...currentCalls}
        }
      }
      else{
        ret = state
      }

      return ret

    default:
      return state
  }
}

export default call
