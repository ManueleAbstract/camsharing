import { combineReducers } from 'redux'
import auth from './auth'
import chat from './chat'
import call from './call'
import registration from './registration'

export default combineReducers({
  auth,
  chat,
  call,
  registration
})
