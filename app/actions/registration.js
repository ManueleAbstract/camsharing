import { CALL_API } from 'redux-api-middleware'
import crypto from 'crypto'

export const saveRegistration = (userInfo) => {
  let formattedUserInfo = userInfo

  //codifica sha256 della password
  const hash = crypto.createHash('sha256')
  hash.update(userInfo.password)
  const codedPassword = hash.digest('hex')

  formattedUserInfo.password = codedPassword

  return {
    [CALL_API]: {
      endpoint: '/api/saveRegistration',
      method: 'POST',
      body: JSON.stringify(formattedUserInfo),
      headers: { 'Content-Type': 'application/json' },
      types: [
        'REGISTRATION_REQUEST',
        {
          type: 'REGISTRATION_RESULT',
          payload: (action, state, res) => {
            return res.json().then(
              () => {
                return {
                  username: formattedUserInfo.username,
                  status: true
                }
              }
            )
          }
        },
        'REGISTRATION_ERROR'
      ]
    }
  }
}

export const checkAccountAvailable = (username, storeFieldName) => {
  return {
    [CALL_API]: {
      endpoint: '/api/getUser',
      method: 'POST',
      body: JSON.stringify({username}),
      headers: { 'Content-Type': 'application/json' },
      types: [
        'ACCOUNT_CHECK_REQUEST',
        {
          type: 'ACCOUNT_CHECK_RESULT',
          payload: (action, state, res) => {
            return res.json().then(
              (resp) => {
                let accountAvailable = false
                if(resp.userFound===false){
                  accountAvailable = true
                }
                return {
                  accountAvailable,
                  storeFieldName
                }
              }
            )
          }
        },
        'ACCOUNT_CHECK_ERROR'
      ]
    }
  }
}

export const confirmRegistration = (username, confirmCode) => {
  let params = {
    username,
    confirmCode
  }

  return {
    [CALL_API]: {
      endpoint: '/api/updateUser',
      method: 'POST',
      body: JSON.stringify(params),
      headers: { 'Content-Type': 'application/json' },
      types: [
        'CONFIRMATION_REQUEST',
        {
          type: 'CONFIRMATION_RESULT',
          payload: (action, state, res) => {
            return res.json().then(
              () => {
                return {
                  confirmed: true
                }
              }
            )
          }
        },
        'CONFIRMATION_ERROR'
      ]
    }
  }
}
