import { CALL_API } from 'redux-api-middleware'

export const saveOwnStream = (streamObj, src) => {
  return {
    type: 'SAVE_STREAM',
    streamObj,
    streamSrc : src
  }
}

export const saveSnapDictionary = (snapDictionary, username) => {
  return {
    type: 'SAVE_SNAP_DICTIONARY',
    snapDictionary,
    username
  }
}

export const updateSnapDictionary = (username, params) => {
  return {
    type: 'UPDATE_SNAP_DICT',
    username,
    params
  }
}

export const initUsersList = (list) => {
  return {
    type: 'INIT_USERS_LIST',
    list
  }
}

export const updateUsersList = (username, userInfo) => {
  return {
    type: 'UPDATE_USERS_LIST',
    username,
    userInfo
  }
}

export const updateUser = (userInfo) => {
  return {
    [CALL_API]: {
      endpoint: '/api/updateUser',
      method: 'POST',
      body: JSON.stringify(userInfo),
      headers: { 'Content-Type': 'application/json' },
      types: [
        'UPDATE_USER_REQUEST',
        {
          type: 'UPDATE_USERS_LIST',
          payload: (action, state, res) => {
            return res.json().then(
              () => {
                return {
                  username: userInfo.username,
                  userInfo
                }
              }
            )
          }
        },
        'UPDATE_USER_ERROR'
      ]
    }
  }
}

export const setSnapTimer = (snapTimer) => {
  return {
    type: 'SET_SNAP_TIMER',
    snapTimer
  }
}

export const newMessage = (unreadMessages) => {
  return {
    type: 'NEW_MESSAGE',
    unreadMessages
  }
}

export const readAllMessages = () => {
  return {
    type: 'MESSAGES_READ'
  }
}
