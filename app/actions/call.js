export const setCall = (interlocutorObj) => {
  return {
    type: 'SET_CALL',
    interlocutorObj
  }
}

export const closeCall = (username) => {
  return {
    type: 'CLOSE_CALL',
    username
  }
}
