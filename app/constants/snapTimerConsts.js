/*let Constants = {
  USER_CONNECTED   : 'user-connected',
  USER_DISCONNECTED: 'user-disconnected',
  USER_MESSAGE     : 'user-message'
};
*/

const snapTimerConsts = {
  MAX_VALUE: 30,
  MIN_VALUE: 5,
  DEFAULT_VALUE: 15
}

export default snapTimerConsts
