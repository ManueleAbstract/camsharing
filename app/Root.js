import React, { Component } from 'react'
import { createStore } from 'redux'
import Router from './Router'
import { Provider } from 'react-redux'
import configureStore from './store/configureStore'

const store = configureStore({})

export default class Root extends Component {
  render(){
    return (
      <Provider store={store}>
        <Router />
      </Provider>
    )
  }
}
