import React from 'react'
import ReactDOM from 'react-dom'
import { connect } from 'react-redux'
import {
  saveSnapDictionary,
  updateSnapDictionary
} from '../../actions/chat'
import {
  Row as RowFlex,
  Col as ColFlex
} from 'react-flexbox-grid'
import {
  SnapCommandsWrapper
} from '../../components/chat'
import {
  SnapContainer
} from '.'

class CamStream extends React.Component {

  constructor(props){
    super(props)
    const chatProxy = this.props.chatProxy

    //binding chatProxy listeners & chatPage callbacks
    chatProxy.saveSnapDictionary(this.saveSnapDictionary.bind(this))
    chatProxy.updateSnapDictionary(this.updateSnapDictionary.bind(this))

    this.userSnapshotsSection = this.userSnapshotsSection.bind(this)
    this.calcImageElem = this.calcImageElem.bind(this)
  }

  calcImageElem(username){
    let snapBox = (
      <SnapContainer
        snapDictionary={this.props.snapDictionary}
        username={username}
        chatProxy={this.props.chatProxy}
        height={this.props.height}
        width={this.props.width}
      />
    )

    let toRender
    if(username === this.props.username){
      toRender = (
        <SnapCommandsWrapper replaceSnapshot={this.props.replaceSnapshot}>
          {snapBox}
        </SnapCommandsWrapper>
      )
    }
    else{
      toRender = snapBox
    }

    return(
      <ColFlex md={3} style={{padding: "0px 1px"}} className="box" key={username}>
        {toRender}
      </ColFlex>
    )
  }

  saveSnapDictionary(snapDictionary){
    this.props.saveSnapDictionary(snapDictionary, this.props.username)
  }

  updateSnapDictionary(username, params){
    this.props.updateSnapDictionary(username, params)
  }

  userSnapshotsSection(){
    let snaps = []
    let toRender
    let camStreamStyle
    const snapDictionary = this.props.snapDictionary

    if(snapDictionary){
      camStreamStyle = "mainSection-chunk mainSection-chunk-camStream"
      Object.keys(snapDictionary).forEach((username) => {
        let elem = (this.calcImageElem(username))
        snaps.push(elem)
      })
    }

    toRender = (<RowFlex id='camStreamRef' className={camStreamStyle} >
                  {snaps}
                </RowFlex>)

    return toRender
  }

  componentWillUpdate(nextProps){
    if(this.props.snapTimer !== nextProps.snapTimer){
      this.props.startTimer(nextProps.snapTimer)
    }
  }

  render(){
    return (
      <div>
        { this.userSnapshotsSection() }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  let ret = {}
  if (state.chat){
    ret = {
      ...ret,
      snapDictionary: state.chat.snapDictionary,
      snapTimer: state.chat.snapTimer
    }
  }
  return ret
}

const mapDispatchToProps = (dispatch) => {
  return {
    saveSnapDictionary: (snapDictionary, username) => {
      dispatch(saveSnapDictionary(snapDictionary, username))
    },
    updateSnapDictionary: (username, params) => {
      dispatch(updateSnapDictionary(username, params))
    }
  }
}

CamStream = connect(mapStateToProps, mapDispatchToProps)(CamStream)

export default CamStream
