import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { connect } from 'react-redux'
import { saveOwnStream } from '../../actions/chat'
import {
  Menu,
  MainSection,
  PublicChatOverlay
} from '../../components/chat'
import { remountComponents } from '../../libs'
import {
  PageHeader,
  Row as RowBoot,
  Col as ColBoot
} from 'react-bootstrap'
import cron from 'node-cron'
import getChatProxyInstance from '../../models/ChatProxy'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

import '../../styles/chat/main.css'
import logoAbstract from '../../../static/logo_abstract.png'


class ChatPage extends Component {

  static contextTypes = {
    router: React.PropTypes.object
  }

  constructor(props){
    super(props)
    this.chatProxy = getChatProxyInstance()
    this.camSize = {
      height: 150,
      width: 200
    }

    this.leavePageHandler = this.leavePageHandler.bind(this)
    this.startTimer = this.startTimer.bind(this)
    this.onTimerTick = this.onTimerTick.bind(this)
    this.handleLogout = this.handleLogout.bind(this)
    this.changeAvailability = this.changeAvailability.bind(this)
  }

  componentWillMount(){
    this.immediateSnapUpdate = true

    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia
    navigator.getUserMedia({ audio: true, video: true },
                             (stream) => {
                               this.chatProxy.connectToServer(this.props.username, true)

                               window.URL = window.URL || window.webkitURL || window.mozURL || window.msURL
                               const src = window.URL.createObjectURL(stream)
                               this.props.saveOwnStream(stream, src)
                               this.camRef = document.getElementById("camRef")
                               this.chatProxy.registerCallListeners(stream)

                               HTMLVideoElement.prototype.initSrc = ('srcObject' in HTMLVideoElement.prototype) ? function(stream) {
                                 this.srcObject = !!stream ? stream : null;
                               } : function(stream) {
                                 this.src = !!stream ? window.URL.createObjectURL(stream) : new String();
                               }

                               this.chatProxy.setCamRef(this.camRef)
                               this.startTimer(this.props.snapTimer)
                               this.immediateSnapUpdate = false
                             },
                             (e) => {
                               this.chatProxy.connectToServer(this.props.username, false)

                               console.log("no user media")
                               this.startTimer(this.props.snapTimer)
                               this.immediateSnapUpdate = false
                             }
                          )
  }

  componentDidMount(){
    this.leavePageHandler()
  }

  leavePageHandler(){
    window.onbeforeunload = () => {
      return "LOGGING OUT..."
    }
    window.onunload = (e) => {
      this.handleLogout(e, true)
    }
  }

  onTimerTick (immediateSnapUpdate, replaceSnaphotCase) {
    let camRef = this.camRef

    if(camRef){
      let thisComponent = this

      camRef.initSrc(this.props.streamObj)
      camRef.onloadedmetadata = (e) => {
        camRef.play()
        camRef.onplaying = () => {
          let done = thisComponent.chatProxy.takeSnapshot(immediateSnapUpdate, replaceSnaphotCase)
          if(done){
            camRef.pause()
          }
        }
      }
    }
    else {
      this.chatProxy.requestSnap()
    }
  }

  startTimer(snapTimer){
    if(this.timer){
      this.timer.stop()
    }

    this.onTimerTick(this.immediateSnapUpdate)

    this.timer = cron.schedule('*/' + snapTimer + ' * * * * *', () => {
      this.onTimerTick(false)
    })
  }

  changeAvailability(availability){
    this.chatProxy.onChangeAvailability(availability)
  }

  handleLogout(e, unloadEvent){
    localStorage.removeItem('username')
    if(this.timer){
      this.timer.stop()
    }
    this.chatProxy.disconnect()
    if(!unloadEvent){
      //caso in cui la funzione è richiamata onunload
      remountComponents(this, 'login')
    }
  }

  render(){
    console.log("my name is", this.props.username)

    return (
      <MuiThemeProvider>
        <div>
          <header className="chatPage-header">
            <div className="container">
              <RowBoot>
                <ColBoot md={2}>
                  <img height="79px" src={logoAbstract}/>
                </ColBoot>
                <ColBoot md={6}>
                  <PageHeader>CamSharing</PageHeader>
                </ColBoot>
                  <ColBoot md={4} id="buttons">
                    <RowBoot>
                      <ColBoot md={9}>
                        <Menu
                          username={this.props.username}
                          chatProxy={this.chatProxy}
                          logout={this.handleLogout}
                          changeAvailability={this.changeAvailability}
                        />
                      </ColBoot>
                      <ColBoot md={3}>
                        <PublicChatOverlay
                          chatProxy={this.chatProxy}
                          username={this.props.username}
                        />
                      </ColBoot>
                    </RowBoot>
                  </ColBoot>
              </RowBoot>
            </div>
          </header>
          <div>
            <video
              muted
              style={{display:"none"}}
              id="camRef"
              width={this.camSize.width}
              height={this.camSize.height}
            />
            <MainSection
              chatProxy={this.chatProxy}
              username={this.props.username}
              startTimer={this.startTimer}
              replaceSnapshot={this.onTimerTick}
              width={this.camSize.width}
              height={this.camSize.height}
            />
          </div>
        </div>
      </MuiThemeProvider>
    )
  }
}

const mapStateToProps = (state) => {
  let ret = {}
  if(state.chat){
    ret = {
      ...ret,
      streamObj : state.chat.streamObj,
      streamSrc : state.chat.streamSrc
    }
  }
  return ret
}

const mapDispatchToProps = (dispatch) => {
  return {
    saveOwnStream: (mediaStreamObj, src) => {
      dispatch(saveOwnStream(mediaStreamObj, src))
    }
  }
}

ChatPage = connect(mapStateToProps, mapDispatchToProps)(ChatPage)

export default ChatPage
