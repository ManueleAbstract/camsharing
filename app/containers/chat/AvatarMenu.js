import React from 'react'
import ReactDOM from 'react-dom'
import { connect } from 'react-redux'
import { AvatarImage } from '../../components/chat'

class AvatarMenu extends React.Component {
  render() {
    return (
      <div>
        <AvatarImage
          size="48"
          src={this.props.avatarImage}
        />
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  let ret = {}
  if (state.chat){
    let usersList = state.chat.usersList
    let username = ownProps.username
    let avatarImage
    if(usersList && usersList[username]){
      avatarImage = usersList[username].avatarImage
    }
    ret = {
      ...ownProps,
      avatarImage
    }
  }
  return ret
}

AvatarMenu = connect(mapStateToProps)(AvatarMenu)

export default AvatarMenu
