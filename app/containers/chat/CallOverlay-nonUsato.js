/*import React, { Component } from 'react'
import Dialog from 'material-ui/Dialog'

class CallOverlay extends Component {

  constructor(props){
    super(props)

    this.state = {
      show: false,
      interlocutorObj: {}
    }

    this.setHeader = this.setHeader.bind(this)
    this.startCall = this.startCall.bind(this)
    this.handleClose = this.handleClose.bind(this)

    const chatProxy = this.props.chatProxy
    chatProxy.startCall(this.startCall)
  }

  startCall(interlocutor_name, interlocutor_stream){
    this.setState({
      show: true,
      interlocutorObj: {
        name: interlocutor_name,
        stream: interlocutor_stream
      }
    })
  }

  setHeader(){
    this.refs.headerMessage.innerText = "In call with " + this.state.interlocutorObj.name
  }

  handleClose(){
    this.setState({
      show: false,
      interlocutorObj: {}
    })
  }

  render() {
    let contentStyle = {
      top: "0%",
      width: "100%",
      maxWidth: "95%",
      padding: "0"
    }

  //  let videoTag = (<Spinner scale={0.50}></Spinner>)
    let videoTag = (<div></div>)
    let headerMessage

    let interlocutor_stream = this.state.interlocutorObj.stream
    if(interlocutor_stream){
      window.URL = window.URL || window.webkitURL || window.mozURL || window.msURL
      const src = window.URL.createObjectURL(interlocutor_stream)
      videoTag = (<video autoPlay src={src} onCanPlayThrough={this.setHeader.bind(this)} />)
      headerMessage = "CALLING " + this.props.interlocutor + "..."
    }

    let dialogStyle
    if(!this.state.show) {
      dialogStyle = {
        display: "none"
      }
    }

    return (
      <div>
        <Dialog
          open={true}
          onRequestClose={this.handleClose}
          style={dialogStyle}
          contentStyle={contentStyle}
          bodyStyle={{padding: "0", overflowY: "hidden", marginBottom: "55px" }}
        >
          <div>
            <h4 ref="headerMessage">
              { headerMessage }
            </h4>
            <div>
              { videoTag }
            </div>
          </div>
        </Dialog>
      </div>
    )
  }
}

export default CallOverlay
*/
