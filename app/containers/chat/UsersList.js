import React from 'react'
import { connect } from 'react-redux'
import {
  initUsersList,
  updateUsersList
} from '../../actions/chat'
import { closeCall } from '../../actions/call'
import {
  AvatarImage,
  PrivateChatOverlay
} from '../../components/chat'
import CommunicationTextsms from 'material-ui/svg-icons/communication/textsms'
import CommunicationCall from 'material-ui/svg-icons/communication/call'
import CommunicationCallEnd from 'material-ui/svg-icons/communication/call-end'
import {redA700} from 'material-ui/styles/colors'
import {
  Row as RowBoot,
  Col as ColBoot
} from 'react-bootstrap'
import {
  List,
  ListItem
} from 'material-ui/List'
import IconButton from 'material-ui/IconButton'

class UsersList extends React.Component {
  componentWillMount(){
    this.updateUserInfo = this.updateUserInfo.bind(this)
    this.initUsersList = this.initUsersList.bind(this)
    this.requestStartCall = this.requestStartCall.bind(this)
    this.requestCloseCall = this.requestCloseCall.bind(this)

    const chatProxy = this.props.chatProxy
    chatProxy.onUserConnected(this.updateUserInfo)
    chatProxy.onUserDisconnected(this.updateUserInfo)
    chatProxy.onChangeAvatar(this.updateUserInfo)
    chatProxy.onUsersList(this.initUsersList)
  }

  initUsersList(list){
    this.props.initUsersList(list)
  }

  updateUserInfo(username, userInfo){
    this.props.updateUsersList(username, userInfo)
  }

  requestStartCall(interlocutor){
    const ownStream = this.props.streamObj
    this.props.chatProxy.requestStartCall(interlocutor, ownStream)
  }

  requestCloseCall(interlocutor){
    this.props.chatProxy.requestCloseCall(interlocutor)
  }

  render() {
    let usersList = this.props.usersList
    let usersTag
    let usersListClass
    let callButton

    const iconStyle = {
      padding: "noPadding",
      width: "noWidth",
      height: "noHeight"
    }

    let toRender = (<List id='usersListRef'></List>)

    if(usersList){
      if(Object.keys(usersList).length > 1){
        //Caso: c'è almeno un altro utente registrato oltre a quello this
        usersListClass = 'mainSection-chunk'
      }

      usersTag = Object.keys(usersList).map((username) => {
        if(username !== this.props.username){
          let itemDisabled = false
          let callDisabled = false
          let itemStyle, tooltipStyle

          let callStyle = {
            ...iconStyle
          }

          if(!usersList[username].logged){
            itemStyle = {
              opacity: 0.5
            }
            itemDisabled = true
          }
          else{
            if(!usersList[username].hasWebcam || !usersList[this.props.username].hasWebcam){
              callDisabled = true
              callStyle = {
                ...callStyle,
                opacity: 0.5
              }
              tooltipStyle = {
                opacity: 0
              }
            }
          }

          if(this.props.currentCalls && this.props.currentCalls[username]){
            callButton = (
              <IconButton
                tooltip={"Hang up call with " + username}
                tooltipPosition="top-left"
                tooltipStyle={tooltipStyle}
                onClick={this.requestCloseCall.bind(this, username)}
                style={iconStyle}
              >
                <CommunicationCallEnd color={redA700}/>
              </IconButton>
            )
          }
          else{
            callButton = (
              <IconButton
                disabled={itemDisabled || callDisabled}
                tooltip={"Call " + username}
                tooltipPosition="top-left"
                tooltipStyle={tooltipStyle}
                style={callStyle}
                onClick={this.requestStartCall.bind(this, username)}
              >
                <CommunicationCall />
              </IconButton>
            )
          }

          const children = (
            <div key={username + "-item"}>
              <RowBoot>
                <ColBoot md={2}>
                  <AvatarImage
                    size="32"
                    src={usersList[username].avatarImage}
                  />
                </ColBoot>
                <ColBoot md={6}>
                  <div>
                    {username}
                    <div className="users-list-item birthname">
                      {usersList[username].firstname + " " + usersList[username].lastname}
                    </div>
                  </div>
                </ColBoot>
                <ColBoot md={2}>
                  {callButton}
                </ColBoot>
                <ColBoot md={2}>
                  <PrivateChatOverlay
                    username={this.props.username}
                    interlocutor={username}
                    chatProxy={this.props.chatProxy}
                  >
                    <IconButton
                      disabled={itemDisabled}
                      tooltip={"Chat with " + username}
                      tooltipPosition="top-left"
                      style={iconStyle}
                    >
                      <CommunicationTextsms />
                    </IconButton>
                  </PrivateChatOverlay>
                </ColBoot>
              </RowBoot>
            </div>
          )

          return (<ListItem
                    key={username}
                    children={children}
                    style={itemStyle}
                    disabled={true}
                  />)
        }
      })

      toRender = (<List id='usersListRef'>
                    {usersTag}
                  </List>)
    }

    return (
      <div className={usersListClass}>
        {toRender}
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  let ret = {}
  if (state.chat){
    ret = {
      ...ownProps,
      usersList: state.chat.usersList,
      streamObj: state.chat.streamObj
    }
  }
  if (state.call){
    ret = {
      ...ret,
      currentCalls: state.call.currentCalls
    }
  }
  return ret
}

const mapDispatchToProps = (dispatch) => {
  return {
    initUsersList: (list) => {
      dispatch(initUsersList(list))
    },
    updateUsersList: (username, userInfo) => {
      dispatch(updateUsersList(username, userInfo))
    },
    closeCall: (username) => {
      dispatch(closeCall(username))
    }
  }
}

UsersList = connect(mapStateToProps, mapDispatchToProps)(UsersList)

export default UsersList
