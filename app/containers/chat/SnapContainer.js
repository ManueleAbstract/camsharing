import React, { Component } from 'react'
import { connect } from 'react-redux'
import ImageBrightness1 from 'material-ui/svg-icons/image/brightness-1'
import {greenA400} from 'material-ui/styles/colors'
import {redA700} from 'material-ui/styles/colors'
import moment from 'moment'
import { momentToDate } from '../../libs/timingUtils'
import ReactTooltip from 'react-tooltip'
import { Image } from 'react-bootstrap'
import { CallOverlay } from '../../components/chat'
import {
  setCall,
  closeCall
} from '../../actions/call'

class SnapContainer extends Component {
  componentWillMount(){
    this.startCall = this.startCall.bind(this)
    this.closeCall = this.closeCall.bind(this)

    const chatProxy = this.props.chatProxy
    chatProxy.onStartCall(this.startCall)
    chatProxy.onCloseCall(this.closeCall)
  }

  shouldComponentUpdate(){
    //Aggiunto controllo per evitare che la componente si renderizzasse durante lo streaming
    return !(this.props.currentCalls && this.props.currentCalls[this.props.username])
  }

  startCall(interlocutor_name, interlocutor_stream){
    if(this.props.username === interlocutor_name){
      //Solo lo SnapContainer che corrisponde all'untente chiamato <interlocutor_name> deve catturare l'evento
      let interlocutorObj = {
        name: interlocutor_name,
        stream: interlocutor_stream
      }

      this.props.setCall(interlocutorObj)
    }
  }

  closeCall(interlocutor_name){
    console.log("ricevo comando di close call");
    if(this.props.username === interlocutor_name){
      console.log("entro per aggiornare", interlocutor_name);
      this.props.closeCall(interlocutor_name)
    }
  }

  render(){
    const username = this.props.username
    const snapItem = this.props.snapDictionary[username]

    let snapOrCall
    let interlocutorObj = (this.props.currentCalls && this.props.currentCalls[username]) ?
                          this.props.currentCalls[username] : null

    if(interlocutorObj && (interlocutorObj.name === this.props.username)){
      snapOrCall = (
        <CallOverlay
          interlocutorObj={interlocutorObj}
          width={this.props.width}
          height={this.props.height}
          username={username}
        />
      )
    }
    else{
      let snapTime = moment(Number(snapItem.time))
      let completeDate = momentToDate(snapTime)

      const iconColor = (snapItem.availability === 'available') ? greenA400 : redA700
      const iconStyle = {
        position: "relative",
        width:"12px",
        height: "noHeight"
      }

      snapOrCall = (
        <div>
          <div data-tip data-for={snapTime.toString()}>
            <Image src={snapItem.snap} />
          </div>
          <ReactTooltip
            id={snapTime.toString()}
            place="bottom"
            effect='solid'
            class='chat-tooltip'
          >
            {completeDate}
          </ReactTooltip>
          <div className="box-username">
            <span className="availability-box">
              <ImageBrightness1 style={iconStyle} color={iconColor}/>
            </span>
            <span>{username}</span>
          </div>
        </div>
      )
    }

    return(
      <div>
        {snapOrCall}
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  let ret = {}
  if (state.call){
    ret = {
      ...ret,
      currentCalls: state.call.currentCalls
    }
  }
  return ret
}

const mapDispatchToProps = (dispatch) => {
  return {
    setCall: (interlocutorObj) => {
      dispatch(setCall(interlocutorObj))
    },
    closeCall: (username) => {
      dispatch(closeCall(username))
    }
  }
}

SnapContainer = connect(mapStateToProps, mapDispatchToProps)(SnapContainer)

export default SnapContainer
