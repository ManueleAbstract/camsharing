import React from 'react'
import { connect } from 'react-redux'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'
import Divider from 'material-ui/Divider'
import ActionAlarm from 'material-ui/svg-icons/action/alarm'
import ContentAdd from 'material-ui/svg-icons/content/add'
import ContentRemove from 'material-ui/svg-icons/content/remove'
import ArrowDropRight from 'material-ui/svg-icons/navigation-arrow-drop-right'
import {
  setSnapTimer
} from '../../actions/chat'
import { snapTimerConsts } from '../../constants'

class SnapTimerSubmenu extends React.Component {

  componentWillMount(){
    this.incrementSnapTimer = this.incrementSnapTimer.bind(this)
    this.decrementSnapTimer = this.decrementSnapTimer.bind(this)
    this.checkSnapTimer = this.checkSnapTimer.bind(this)
  }

  checkSnapTimer(a){
    let ret = a
    if(!a){
      //Valore di default di a è 5 minuti
      ret = snapTimerConsts.DEFAULT_VALUE
    }
    return ret
  }

  incrementSnapTimer(a, b){
    let ret
    let sum
    a = this.checkSnapTimer(a)
    sum = a + b
    if(sum <= snapTimerConsts.MAX_VALUE){
      ret = sum
    }
    else{
      ret = a
    }
    return ret
  }

  decrementSnapTimer(a, b){
    let ret
    a = this.checkSnapTimer(a)
    let c = a - b
    if (c >= snapTimerConsts.MIN_VALUE){
      ret = c
    }
    else{
      ret = a
    }
    return ret
  }

  render(){

    const timingCases = [
      {
        label: "1 second",
        val: 1
      },
      {
        label: "2 seconds",
        val: 2
      },
      {
        label: "5 seconds",
        val: 5
      }
    ]

    const menuItemCases = (icon, cb) => {
      let list = []
      let item
      let numCases = timingCases.length
      let index
      let newVal

      for (index in timingCases){
        newVal = cb(this.props.snapTimer, timingCases[index].val)
        item = <MenuItem
                  primaryText={timingCases[index].label}
                  leftIcon={icon}
                  onClick={this.props.setSnapTimer.bind(this, newVal, this.props.handleRequestClose)}
                />
        list.push(item)
        if(index < (numCases-1)){
          list.push(<Divider />)
        }
      }
      return list
    }

    const labelToShow = "Timer (" + this.props.snapTimer + " sec)"
    return(
      <MenuItem
        leftIcon={<ActionAlarm />}
        primaryText={labelToShow}
        rightIcon={<ArrowDropRight />}
        menuItems={[
          <MenuItem
            primaryText={"Reset default: "+snapTimerConsts.DEFAULT_VALUE+" sec"}
            onClick={this.props.setSnapTimer.bind(this, snapTimerConsts.DEFAULT_VALUE, this.props.handleRequestClose)}
          />,
          <Divider />,
          <MenuItem
            primaryText="Increment"
            rightIcon={<ArrowDropRight />}
            menuItems={menuItemCases(<ContentAdd />, this.incrementSnapTimer)}
          />,
          <Divider />,
          <MenuItem
            primaryText="Decrement"
            rightIcon={<ArrowDropRight />}
            menuItems={menuItemCases(<ContentRemove />, this.decrementSnapTimer)}
          />
        ]}
      />
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  let ret = {}
  if (state.chat){
    ret = {
      ...ownProps,
      snapTimer: state.chat.snapTimer
    }
  }
  return ret
}

const mapDispatchToProps = (dispatch) => {
  return {
    setSnapTimer: (snapTimer, closeMenu) => {
      closeMenu()
      dispatch(setSnapTimer(snapTimer))
    },
  }
}

SnapTimerSubmenu = connect(mapStateToProps, mapDispatchToProps)(SnapTimerSubmenu)

export default SnapTimerSubmenu
