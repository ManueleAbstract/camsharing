import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { connect } from 'react-redux'
import { remountComponents } from '../../libs'
import { confirmRegistration } from '../../actions/registration'

class ConfirmRegistration extends Component {

  static contextTypes = {
    router: React.PropTypes.object
  }
  
  componentDidMount(){
    let urlParams = this.props.location.query
    this.username = urlParams.username
    this.props.confirmRegistration(urlParams.username, urlParams.confirmCode)
  }

  componentDidUpdate(){
    if(this.props.confirmed){
      remountComponents(this, 'login')
    }
  }

  render(){
    return (
      <div></div>
    )
  }
}

const mapStateToProps = (state) => {
  let ret = {}
  if(state.registration){
    ret = {
      ...ret,
      confirmed: state.registration.confirmed
    }
  }
  return ret
}

const mapDispatchToProps = (dispatch) => {
  return {
    confirmRegistration: (username, confirmCode) => {
      dispatch(confirmRegistration(username, confirmCode))
    }
  }
}

ConfirmRegistration = connect(mapStateToProps, mapDispatchToProps)(ConfirmRegistration)


export default ConfirmRegistration
