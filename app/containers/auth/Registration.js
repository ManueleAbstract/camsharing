import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { connect } from 'react-redux'
import {
  saveRegistration
} from '../../actions/registration'
import {
  Input,
  Form,
  PasswordWidget,
  UsernameField,
  EmailField
} from '../../components/formWidgets'
import { Cropper } from '../../components/genericWidgets'
import NotificationSystem from 'react-notification-system'
import {
  remountComponents,
  checkFormValidations
} from '../../libs'
import { Link } from 'react-router'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import injectTapEventPlugin from 'react-tap-event-plugin'

import '../../styles/auth/main.css'

//Necessario per l'evento click sulle componenti di material-ui
injectTapEventPlugin()

class MyLink extends Link {
  constructor(props){
    super(props)
    this.submitted = false //Flag per evitare che si possa cliccare ripetutamente sul tasto di registrazione
    this.customHandleClick = this.customHandleClick.bind(this)

    this.originalHandleClick = this.handleClick
    this.handleClick = this.customHandleClick
  }

  customHandleClick(e){
    let value = confirm("Warning: all inserted data will be lost. Continue?")
    if(value === true){
      this.originalHandleClick(e)
    }
    else{
      e.preventDefault()
    }
  }
}

class Registration extends Component {

  mapInputs(inputs) {
    return {
      firstname: inputs.firstname,
      lastname: inputs.lastname,
      username: inputs.username,
      password: inputs.password,
      email: inputs.email,
      avatarImage: undefined
    }
  }

  static contextTypes = {
    router: React.PropTypes.object
  }

  constructor(props){
    super(props)
    this.state = {}

    this.onSubmit = this.onSubmit.bind(this)
    this.setAvatarImage = this.setAvatarImage.bind(this)
  }

  componentWillMount(){
    this.props.resetStore()
    window.onbeforeunload = () => {
      return "Warning: all inserted data will be lost!"
    }
  }

  setAvatarImage(avatarImage){
    this.setState({
      avatarImage
    })
  }

  onSubmit(formValues){
    if(!this.submitted){
      if(checkFormValidations(this, 'form-reg')){
        this.submitted = true
        formValues.avatarImage = this.state.avatarImage
        this.props.saveRegistration(formValues)
      }
      else{
        this.notifAppearsReg.addNotification({
          message: 'Form not correctly filled',
          title: 'Invalid Values!',
          level: 'error'
        })
      }
    }
  }

  componentWillUpdate(nextProps){
    if(nextProps.registrationStatus){
      remountComponents(this, 'registrationSubmitted')
    }
  }

  render(){
    return (
      <MuiThemeProvider>
        <div className="auth-page">
          <NotificationSystem ref={(ref) => this.notifAppearsReg = ref} />
          <div className="pen-title">
            <h1>CamSharing Portal</h1>
          </div>
          <div className="form-module">
            <div className="form">
              <h2>Create an account</h2>
              <Form
                mapping={this.mapInputs}
                onSubmit={this.onSubmit}
                ref="form-reg"
              >
                <Input
                  type="text"
                  placeholder="Firstname*"
                  name="firstname"
                  validations="isAlpha"
                  validations={{
                    isAlpha:"isAlpha",
                    maxLength: 15
                  }}
                  validationErrors={{
                    isAlpha: 'String can only constain a-z characters.',
                    maxLength: 'Too long string, max 15 characters.'
                  }}
                  value=""
                  preventSpaces
                  fullwidth
                  required
                />
                <Input
                  type="text"
                  placeholder="Lastname*"
                  name="lastname"
                  validations={{
                    isAlpha:"isAlpha",
                    maxLength: 15
                  }}
                  validationErrors={{
                    isAlpha: 'String can only constain a-z characters.',
                    maxLength: 'Too long strig, max 15 characters.'
                  }}
                  value=""
                  preventSpaces
                  fullwidth
                  required
                />
                <UsernameField name={"username"}/>
                <Cropper setAvatarImage={this.setAvatarImage}/>
                <PasswordWidget name={"password"}/>
                <EmailField name={"email"}/>
                <button
                  type="submit"
                  className="btn btn-primary"
                  disabled={!this.props.accountAvailable || !this.props.emailAvailable}
                >
                  Register
                </button>
              </Form>
            </div>
            <div className="cta">
              <MyLink
                to="login"
              >
                &lt;&nbsp;Back
              </MyLink>
            </div>
          </div>
        </div>
      </MuiThemeProvider>
    )
  }
}

const mapStateToProps = (state) => {
  let ret = {}
  if(state.registration){
    ret = {
      registrationStatus: state.registration.status,
      accountAvailable: state.registration.accountAvailable,
      emailAvailable: state.registration.emailAvailable
    }
  }
  return ret
}

const mapDispatchToProps = (dispatch) => {
  return {
    saveRegistration: (credentials) => {
      dispatch(saveRegistration(credentials))
    },
    resetStore: () => {
      dispatch({type:'RESET_STORE'})
    }
  }
}

Registration = connect(mapStateToProps, mapDispatchToProps)(Registration)

export default Registration
