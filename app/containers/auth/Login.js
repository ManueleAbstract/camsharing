import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { connect } from 'react-redux'
import { tryLogin } from '../../actions/auth'
import {
  remountComponents,
  checkFormValidations
} from '../../libs'
import {
  Form,
  Input
} from '../../components/formWidgets'
import NotificationSystem from 'react-notification-system'
import { Link } from 'react-router'
import crypto from 'crypto'

class Login extends Component {

  static contextTypes = {
    router: React.PropTypes.object
  }

  constructor(props){
    super(props)

    this.onSubmit = this.onSubmit.bind(this)
  }

  onSubmit(formValues){
    if(checkFormValidations(this, 'form-login')){
      const hash = crypto.createHash('sha256')
      hash.update(formValues.password)
      const codedPassword = hash.digest('hex')

      this.props.tryLogin(formValues.username, codedPassword)
    }
    else{
      this.notifAppearsLogin.addNotification({
        message: 'Form not correctly filled',
        title: 'Invalid Values!',
        level: 'error'
      })
    }
  }

  componentWillMount(){
    this.props.resetStore()
    if(window.onbeforeunload){
      window.onbeforeunload = undefined
    }
  }

  componentWillUpdate(nextProps){
    if(localStorage.getItem('username')){
      remountComponents(this, 'chat')
    }
    if(nextProps.userFound===false){
      this.notifAppearsLogin.addNotification({
        message: 'Wrong credentials',
        title: "Error!",
        level: 'error',
        uid: "wrong_credentials"
      })
    }
  }

  render(){
    return (
      <div className="auth-page">
        <NotificationSystem ref={(ref) => this.notifAppearsLogin = ref} />
        <div className="pen-title">
          <h1>CamSharing Portal</h1>
        </div>
        <div className="form-module">
          <div className="form">
            <h2>Login to your account</h2>
            <Form
              onSubmit={this.onSubmit}
              ref="form-login"
            >
              <Input
                type="text"
                name="username"
                placeholder="Username or Email*"
                hideSuccessStatus
                preventSpaces
                fullwidth
                required
              />
              <Input
                type="password"
                name="password"
                placeholder="Password*"
                hideSuccessStatus
                preventSpaces
                fullwidth
                required
              />
              <button
                type="submit"
                className="btn btn-primary"
              >
                Login
              </button>
            </Form>
          </div>
          <div className="cta">
            <Link
              to="registration"
            >
              get an account
            </Link>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  let ret = {}
  if(state.auth){
    ret = {
      ...ret,
      userFound: state.auth.userFound
    }
  }
  if(state.registration){
    ret = {
      ...ret,
      username: state.registration.username
    }
  }
  return ret
}

const mapDispatchToProps = (dispatch) => {
  return {
    tryLogin: (username, password) => {
      dispatch(tryLogin(username, password))
    },
    resetStore: () => {
      dispatch({type:'RESET_STORE'})
    }
  }
}

Login = connect(mapStateToProps, mapDispatchToProps)(Login)

export default Login
