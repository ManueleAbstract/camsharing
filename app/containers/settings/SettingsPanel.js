import React, {Component} from 'react'
import { connect } from 'react-redux'
import Drawer from 'material-ui/Drawer'
import MenuItem from 'material-ui/MenuItem'
import RaisedButton from 'material-ui/RaisedButton'
import Slider from 'material-ui/Slider'
import {
  List,
  ListItem
} from 'material-ui/List'
import { Cropper } from '../../components/genericWidgets'
import { snapTimerConsts } from '../../constants'
import {
  updateUser
} from '../../actions/chat'

class SettingsPanel extends React.Component {
  componentDidMount(){
    this.updateAvatar = this.updateAvatar.bind(this)
  }

  updateAvatar(newAvatar){
    const username = this.props.username
    let userInfo = {}
    userInfo.avatarImage = newAvatar

    //aggiorna l'avatar su tutti gli altri peers
    this.props.chatProxy.broadcastAvatarChanged(username, newAvatar)

    //L'API di salvataggio su DB vuole un oggetto unico con il campo username
    userInfo.username = username
    //Aggiorna l'avatar localmente
    this.props.updateUser(userInfo)
  }

  render() {
    const panelWidth = (window.screen.width) * 0.35
    const currentAvatar = this.props.currentAvatar
    const username = this.props.username
    /*let currentAvatar
    if(usersList && usersList[username]){
      currentAvatar = usersList[username].avatarImage
    }*/

    return (
      <div>
        <Drawer
          docked={false}
          width={200}
          open={this.props.openedSettings}
          onRequestChange={this.props.handleClose}
          width={panelWidth}
          containerStyle={{zIndex: "1000 !important"}}
          overlayStyle={{zIndex: "1000 !important"}}
        >
          <List>
            <ListItem
              primaryText="Registration Settings"
              initiallyOpen={true}
              primaryTogglesNestedList={true}
              nestedItems={[
                <Cropper
                  key="cropper"
                  currentAvatar={currentAvatar}
                  setAvatarImage={this.updateAvatar}
                />
              ]}
            />
          {/*
            <ListItem
              primaryText="Snap Timer"
              initiallyOpen={false}
              primaryTogglesNestedList={true}
              nestedItems={[
                <MySlider />
              ]}
            />*/}
          </List>
        </Drawer>
      </div>
    )
  }
}

class MySlider extends Component{
  constructor(props){
    super(props)

    this.state = {
      sliderValue: snapTimerConsts.DEFAULT_VALUE
    }

    this.handleValueChange = this.handleValueChange.bind(this)
  }

  handleValueChange(event, value) {
    this.setState({sliderValue: value});
  }

  render(){
    const currentValueAdvice = "The value of this slider is: "+this.state.sliderValue+
                               " from a range of "+snapTimerConsts.MIN_VALUE+
                               " sec to "+snapTimerConsts.MAX_VALUE+" (inclusive)."

    return (
      <div style={{padding:"20px"}}>
        <Slider
          min={snapTimerConsts.MIN_VALUE}
          max={snapTimerConsts.MAX_VALUE}
          defaultValue={snapTimerConsts.DEFAULT_VALUE}
          step={1}
          value={this.state.sliderValue}
          onChange={this.handleValueChange}
          sliderStyle={{marginBottom:"10px"}}
        />
        <p>
          <span>{currentValueAdvice}</span>
        </p>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  let ret = {}
  if (state.chat && state.chat.usersList){
    let list = state.chat.usersList
    let username = ownProps.username
    ret = {
      ...ownProps,
      currentAvatar: list[username].avatarImage
    }
  }
  return ret
}

const mapDispatchToProps = (dispatch) => {
  return {
    updateUser: (userInfo) => {
      dispatch(updateUser(userInfo))
    }
  }
}

SettingsPanel = connect(mapStateToProps, mapDispatchToProps)(SettingsPanel)

export default SettingsPanel
