'use strict'

var options = require('../libs/options')

const snapshotsAPICall = {
  saveSnapshot: (apiInvoker, address, data) => {
    return apiInvoker(options('POST', address+"/api/saveSnapshot", data))
      .then(function (resp) {
        return resp
      })
      .catch(function (err) {
        console.log("saveSnapshot failed");
      })
  },
  getSnapshotsList: (apiInvoker, address) => {
    return apiInvoker(options('GET', address+'/api/getSnapshotsList'))
      .then(function (resp) {
        let ret
        if(!resp.length){
          ret = {}
        }
        else{
          let formattedItem = {}
          resp.map((item)=>{
            formattedItem[item.username] = item.snapObj
          })
          ret = formattedItem
        }
        return ret
      })
      .catch(function (err) {
        console.log("getSnapshotList failed");
      })
  }
}

module.exports = snapshotsAPICall
