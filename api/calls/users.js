'use strict'

var options = require('../libs/options')

const usersAPICall = {
  getUsersList: (apiInvoker, address, username) => {
    let resource = address+'/api/getUsersList'
    if(username){
      resource += '/'+username
    }
    return apiInvoker(options('GET', resource))
      .then(function (resp) {
        return resp
      })
      .catch(function (err) {
        console.log("getUsersList failed");
      })
  }
}

module.exports = usersAPICall
