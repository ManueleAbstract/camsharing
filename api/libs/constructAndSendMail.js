'use strict'

var nodemailer = require('nodemailer')
var path = require('path')
var os = require("os")

const constructAndSendMail = (userInfo, send_callback) => {
  let transport = nodemailer.createTransport(
    {
      service: 'gmail',
      auth: {
        user: 'abstract.prova@gmail.com',
        pass: 'X@FcdxpD:E(=|$'
      }
    }
  )

  let host = getIp() || "localhost"
  let port = "3001"
  const address = 'http://' + host + ':' + port
  let apiFolder = path.parse(__dirname).dir

  let sendingParams = {
    address,
    apiFolder
  }

  send_callback(userInfo, transport, sendingParams)
}

function getIp (){
  var interfaces = os.networkInterfaces();
  var addresses = [];
  for (var k in interfaces) {
      for (var k2 in interfaces[k]) {
          var address = interfaces[k][k2];
          if (address.family === 'IPv4' && !address.internal) {
              addresses.push(address.address);
          }
      }
  }
  console.log("Conf mail, ip:", addresses);
  return addresses[0]
}

module.exports = constructAndSendMail
