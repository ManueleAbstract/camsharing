'use strict'

var fs = require('fs')

const sendConfirmationMail = (userInfo, transport, sendingParams) => {
  fs.readFile(sendingParams.apiFolder+'/templates/confirmationMail.html', function (err, html) {
    if (err) {
      throw err
    }

    html = html.toString()
    html = html.replace("#birthName", userInfo.firstname+" "+userInfo.lastname)
    html = html.replace("#confirmationLink", sendingParams.address+"/#/confirmRegistration?username="+userInfo.username+"&confirmCode="+userInfo.confirmCode)

    let email = {
      from: 'CamSharing <no-reply@camsharing.com>',
      to: userInfo.email,
      subject: 'CamSharing: please confirm your subscription',
      html
    }

    transport.sendMail(email, (err, responseStatus) => {
      if (err) {
        console.log(err)
      } else {
        console.log("Confirmation mail sent to", userInfo.email)
      }
    })
  })
}

module.exports = sendConfirmationMail
