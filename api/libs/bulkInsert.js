const bulkInsert = function(models, fn) {
  if (!models || !models.length){
    return fn(null)
  }

  var bulk = this.collection.initializeOrderedBulkOp()
  if (!bulk)
    return fn('bulkInsertModels: MongoDb connection is not yet established')

  var model
  for (var i=0; i<models.length; i++) {
    model = models[i]
    bulk.insert(model.toJSON())
  }

  bulk.execute(fn)
}

module.exports = bulkInsert

//Lo scopo di bulkInsert è di inserire una lista di oggetti eseguendo un'unica operazione di insert
