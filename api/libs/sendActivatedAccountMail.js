'use strict'

var fs = require('fs')

const sendConfirmationMail = (userInfo, transport, sendingParams) => {

  fs.readFile(sendingParams.apiFolder+'/templates/activatedAccountMail.html', function (err, html) {
    if (err) {
      throw err
    }

    html = html.toString()
    html = html.replace("#username", userInfo.username)
    html = html.replace("#camsharingLink", sendingParams.address)

    let email = {
      from: 'CamSharing <no-reply@camsharing.com>',
      to: userInfo.email,
      subject: 'CamSharing: account activated',
      html
    }

    transport.sendMail(email, (err, responseStatus) => {
      if (err) {
        console.log(err)
      } else {
        console.log("Activated account mail sent to", userInfo.email)
      }
    })
  })
}

module.exports = sendConfirmationMail
