"use strict"

var MessagesSchema = require('../../schemas/messages')
var mongoose = require('mongoose')

const messagesAPI = {
  saveMessage: (req, res) => {
    const fields = req.body
    const item = new MessagesSchema(fields)

    item.save(function(err) {
      if (err) {
        res.send(err)
      }
      else{
        res.json(item)
      }
    })
  },
  saveAllMessages: (req, res) => {
    const messagesList = req.body
    let bulk = []
    messagesList.map((message)=>{
      const item = new MessagesSchema(message)
      bulk.push(item);
    })
    MessagesSchema.bulkInsert(bulk, function(err) {
      if (err) {
        res.send(err)
      }
    })
  },
  getMessagesList: (req, res) => {
    MessagesSchema.find().sort({'time': 1}).limit(50).exec(function(err, list) {
      if (err){
        res.send(err)
      }
      else{
        res.json(list)
      }
    })
  }
}

module.exports = messagesAPI
