'use strict'

var moment = require('moment')
var crypto = require('crypto')
var UsersSchema = require('../../schemas/users')
var onlyFirstLetterCapitalized = require ('../../libs/stringUtils')
var constructAndSendMail = require ('../libs/constructAndSendMail')
var sendConfirmationMail = require ('../libs/sendConfirmationMail')
var sendActivatedAccountMail = require ('../libs/sendActivatedAccountMail')

const now = () => {
  return (+moment()).toString()
}

const random = () => {
  return ((Math.random() / Math.random()) * Math.random()).toString()
}

const usersAPI = {
  saveRegistration: (req, res) => {
    const fields = req.body
    let user

    const hash = crypto.createHash('sha256')
    hash.update(random())

    fields.firstname = onlyFirstLetterCapitalized(fields.firstname)
    fields.lastname = onlyFirstLetterCapitalized(fields.lastname)
    fields.registrationDate = now()
    fields.confirmCode = hash.digest('hex')
    fields.confirmedReg = false

    user = new UsersSchema(fields)
    user.save((err) => {
      if (err) {
        res.send(err)
      }
      else{
        constructAndSendMail(fields, sendConfirmationMail)
        res.json(user)
      }
    })
  },
  updateUser: (req, res) => {
    const fields = req.body
    let username = fields.username
    let confirmCode = fields.confirmCode
    let user

    UsersSchema.find({username}, function(err, docs){
      if(!err){
        if(docs.length){
          let confirmRegistrationCase = confirmCode ? true : false
          let userToUpdate = docs[0]

          if(confirmRegistrationCase && userToUpdate.confirmedReg===true){
            /*
              Non viene fatta alcuna modifica se l'account è stato già attivato.
              Caso in cui si clicca più volte su link inviato per mail
            */
            return
          }

          Object.keys(fields).forEach((fieldName) => {
            userToUpdate[fieldName] = fields[fieldName]
          })

          if(confirmRegistrationCase && (userToUpdate.confirmCode === confirmCode)){
            //Si accede nel caso in cui l'utente clicca sul link inviatogli per mail
            userToUpdate.confirmedReg = true
          }
          user = new UsersSchema(userToUpdate)
          user.save((err) => {
            if (err) {
              res.send(err)
            }
            else{
              if(confirmRegistrationCase){
                constructAndSendMail(userToUpdate, sendActivatedAccountMail)
              }
              res.json(user)
            }
          })
        }
        else{
          console.log("User research got undefined");
        }
      }
      else {
        res.send(err)
      }
    })
  },
  getUsersList: (req, res) => {
    let filter = {
      confirmedReg: true
    }
    let usernameParam = req.params.username
    if(usernameParam){
      filter.username = usernameParam
    }
    UsersSchema.find(filter).select("firstname lastname username avatarImage").exec(function(err, list) {
      if (err){
        res.send(err)
      }
      else{
        let formattedList = {}
        list.forEach((val) => {
          formattedList[val.username] = {
            firstname: val.firstname,
            lastname: val.lastname,
            avatarImage: val.avatarImage,
            logged: false
          }
        })
        if(usernameParam){
          res.json(formattedList[usernameParam])
        }
        else{
          res.json(formattedList)
        }
      }
    })
  },
  getUser: (req, res) => {
    /*
      Se nel body è specificato anche il parametro password allora questo viene
      preso in considerazione per il fetch dal db, altrimenti no.
      getUser è usata sia per controllare se uno username già esiste (Registrazione),
      sia per controllare le credenziali di autenticazione (Login).
    */
    const username = req.body.username
    const usernameCaseIns = new RegExp("^"+username+"$", "i")

    let findCriteria = {
      "$or": [
        {username: usernameCaseIns},
        {email: usernameCaseIns}
      ]
    }

    /*
      Non è possibile (per via dei controlli sulla form) che un account abbia username = email.
      In caso contrario esisterebbero situazioni particolari che invaliderebbero il risultato.
    */

    if (req.body.password){
      const password = req.body.password
      findCriteria = {
        "$and": [
          {password},
          {"confirmedReg": true},
          findCriteria
        ]
      }
    }

    UsersSchema
      .find(findCriteria)
      .exec((err, docs) => {
        if(!err && docs.length){
          res.json({
            username: docs[0].username,
            userFound: true
          })
        }
        else if (!docs.length){
          res.json({
            userFound: false
          })
        }
      })
  }
}

module.exports = usersAPI
