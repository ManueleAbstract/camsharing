var mongoose = require('mongoose')
var Schema = mongoose.Schema
var bulkInsert = require('../api/libs/bulkInsert')

var MessagesSchema = new Schema({
  author: String,
  content: String,
  time: String
})


var model = mongoose.model('messagesSchema', MessagesSchema)

model.bulkInsert = bulkInsert

module.exports = model
