var mongoose = require('mongoose')

var Schema = mongoose.Schema

var UsersSchema = new Schema({
  firstname: String,
  lastname: String,
  username: String,
  password: String,
  email: String,
  avatarImage: String,
  registrationDate: String,
  confirmCode: String,
  confirmedReg: Boolean
})

module.exports = mongoose.model('usersSchema', UsersSchema)
